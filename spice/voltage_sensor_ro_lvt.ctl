*line one of a control file must be a comment (HSPICE syntax rules)
* ==========================
*      File Includes
* ==========================


.PARAM tsim = 800n
.INC "outputs/temp_vdd.txt"
.INC "../netlist/voltage_sensor_ro_lvt.ckt"


* ==========================
*   Simulation Parameters
* ==========================
* - essentially variables
* - HSPICE keywords are case insensitive (.param, .PARAM, etc.)
.PARAM vss =0
.PARAM tsim = 800n
Vvsupply VDD VSS vdd
Vvss VSS 0 vss
Vvh HOLD 0 PWL(0 0 10n 0 10.01n vdd)
* ==========================
*    Simulation Duration
* ==========================
* - transient simulation


* Generate the lis file correctly
.OPTION MEASFORM = 1
.OPTION BRIEF
*.OPTION POST
*.OPTION MTTHRESH=3

*.MEAS vo_avg AVG V(Vout) from 0 to 'tsim/4'
 *measure the comparator signal period


.MEAS TRAN period
        + TRIG V(Vout) VAL='vdd/2' TD=20n RISE = 2
        + TARG V(Vout) VAL='vdd/2' RISE = 4

.MEAS TRAN frequency PARAM  = '2/period'



.OPTION AUTOSTOP='period'

 .END
