clc
clear
close all
reg_order = 3;

M = csvread('../data/observed_data_ro.csv');
%M = csvread('../data/voltage_sensor_ro_lvt.csv');

T = M(:,1);
vdd = M(:,2)*1e3;
Freq = 1e-6*M(:,3); %% in mu seconds
%Freq = 1./Period;

T = T/max(abs(T));
vdd = vdd/max(vdd);
Freq = Freq/max(Freq);


T0 = unique(T); %% Temperature points where I want to calculate the j gradiant
V0 = unique(vdd); %% voltage points where I want to calculate the i gradiant
dfdv = [];
dfdT = [];
%%%%%%% j gradiant %%%%%%%
for j=1:1:length(T0)

    f0 = Freq(find(T==T0(j)));
    %dfdv = [0; diff(f0)./diff(V0)];
    dfdv = [dfdv; gradient(f0,V0(2)-V0(1))];

end

for i=1:1:length(V0)

    f0 = Freq(find(vdd==V0(i)));
    dfdT = [dfdT; gradient(f0,T0(2)-T0(1))];


end

[Tg,Vg] = meshgrid(T,vdd);
grad = meshgrid(dfdT,dfdv);
Frequ= griddata(T,vdd,Freq,Tg,Vg);

%surf(Tg,Vg,grad);
contour(Tg,Vg,Frequ);
hold on
quiver(T,vdd,dfdT,dfdv);