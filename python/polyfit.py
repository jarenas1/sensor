import numpy as np
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline
from sklearn.linear_model import TweedieRegressor
import matplotlib.pyplot as plt
import sys
from scipy.optimize import minimize

def target(X,coefficients):
    x = X[:,1:]
    x0 = X[:,0]
    y = np.matmul(x,coefficients) + x0
    return y

def lare(y_pred,y_true):
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    assert len(y_pred)==len(y_true)
    if np.any(y_true<= 0):
        print("Found negative and/or zero values in y_true, LARE undefined. Removing from the set.")
        idx = np.where(y_true<=0)
        y_true = np.delete(y_true,idx)
        y_pred = np.delete(y_pred,idx)
    
    rel_error_a = np.sum(np.abs(y_pred-y_true))
    rel_error_b = np.abs((y_pred-y_true)/y_pred)
    #obj_rel_error = np.mean(rel_error_a+rel_error_b)
    obj_rel_error = np.mean(rel_error_a)
    return obj_rel_error

def cost_function(coefficients,X,Y):
    #error = lare(np.matmul(X,coefficients),Y)
    error = np.sum((Y-target(X,coefficients)/Y)**2)
    return (error)


obs_data = np.loadtxt("../data/voltage_sensor_ro_lvt.csv",dtype=float,delimiter=",")
#in_file = "../data/voltage_sensor_ro_lvt_coefficients.csv"

obs_data = np.loadtxt("../data/temp_sensor.csv",dtype=float,delimiter=",")
in_file = "../data/temp_sensor_coefficients.csv"
iterations = np.shape(obs_data)[0]
samples = np.shape(obs_data)[1] - 2
TEMP = obs_data[:,0]
VDD = obs_data[:,1]*1e3

Freq = np.log10(obs_data[:,2:]*1e-6)

#Freq = (obs_data[:,2:]*1e-6)
try:
    Freq1 = Freq[:,1]
except:
    Freq1 = Freq
#print(VDD)
#print(TEMP)
#print(Freq)
#print(Freq[:,0])
#print(Freq.shape)
#print(Freq1.shape)

X = obs_data[:,0:2]
X[:,1] = X[:,1]*1e3
#print(X)
#print(X.shape)
poly = PolynomialFeatures(degree=3)
Xpoly = poly.fit_transform(X)
#print(Xpoly)
#print(Xpoly.shape)
#model = linear_model.LinearRegression() #This fits using the standard LSE (min(y-yhat)2)#model = linear_model.Ridge(alpha=1) #This fits using the standard LSE (min(y-yhat)2)
model = linear_model.Ridge(alpha=10) #This fits using the standard LSE (min(y-yhat)2)
#model = TweedieRegressor(power=0,alpha=0) #This fits using the inverse gausian distribution (using the relative error)
flag = False
#print(model)
head = "A0,T,V,T^2,V^2,T^3,TV,T^2V,TV^2,V^3"

beta_initial = np.array([1]*(Xpoly.shape[1]-1))
print(beta_initial)
for i in range(0,samples):
    
    #result = minimize(cost_function,beta_initial,args=(Xpoly,Freq[:,i]),method='SLSQP',options={'maxiter':1e4})
    result = minimize(cost_function,beta_initial,args=(Xpoly,Freq[:,i]),method='SLSQP',options={'maxiter':1e4})
    model.fit(Xpoly,Freq[:,i])
    Fhat = model.predict(Xpoly) 
    coefficients = model.coef_
    c0 = model.intercept_
    cx = np.append([c0],coefficients[1:10],axis=0)
    np.savetxt('dd.txt',Fhat)
    np.savetxt('cc.txt',Freq)

    Error = (Freq[:,i]-Fhat)/Freq[:,i] * 100
    Error = np.absolute(Error)
    
    Fhat_lare = target(Xpoly,result.x)
    #print(Freq[:,i])
    #print(Fhat_lare)
    
    print(result)
    print(coefficients)
    Error_lare = np.abs((Freq[:,i]-Fhat_lare)/Freq[:,i] * 100)

    print("the maximum error is: ")
    print(np.amax(Error))
    print("the lare error is: ")
    print(np.amax(Error_lare))
    #print("The coeff are:")
    #print(coefficients)
    #print("The rel coeff are:")
    #print(result.x)
    #print("XXXXXXXXXXXXX RESULT XXXXXXXXXXXXXXX")
    #print(result.fun)
    #print(c0)
    
    
    if flag== False:
        np.savetxt(in_file,[cx],delimiter=",",fmt='%1.5e',header=head)
        flag = True
    else:
        with open(in_file,'ab') as f:
            np.savetxt(f,[cx],delimiter=",",fmt='%1.5e')
        f.close()
        #array_matrix = np.concatenate((array_matrix,array_line),axis=0) 
        #Concatenate the matrix with the previous run matrix
 

#print(Freq)

sys.exit()
plt.plot(Freq,Freq,color="black")
plt.scatter(Freq,Fhat,color="red")
plt.show()
