clc
clear all
close all
X = 1:15;
Y = [5 5 5 10 15 20 25 25 25  20 15 10 5 5 5];
dx = 1;
dy = 1;
Z = (ones(numel(X),1)*Y)+ randn(15,15);
figure
surf(Z)
title('surface');
figure
surf(gradient(Z))
title('gradient');