import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline


ideal_source = np.loadtxt("../data/7s_ideal_20neg.csv",dtype=float,delimiter=",")

Tbuff = np.loadtxt("../data/7s_E_20neg.csv",dtype=float,delimiter=",")

Tis = ideal_source[0,0]
VDDis = ideal_source[:,1]
Fis = np.sort(ideal_source[:,2],axis=None)

T3t = Tbuff[0,0]
VDD3t = Tbuff[:,1]
F3t = np.sort(Tbuff[:,2],axis=None)

poly = PolynomialFeatures(degree=2)
Xp=np.stack((VDD3t,np.zeros(VDD3t.shape)),axis=1)

X = poly.fit_transform(Xp)
model = linear_model.LinearRegression()

model.fit(X,F3t)
Fhat = model.predict(X)
biggest_error  = np.amax((F3t-Fhat)/F3t*100)
smallest_error = np.amin((F3t-Fhat)/F3t*100)

print(biggest_error)
print(smallest_error)



fig,ax1 = plt.subplots()

ax1.semilogy(VDDis*1e3,(Fis/np.amax(Fis)),color=[0,0,1],label="Ideal Source")
ax1.semilogy(VDD3t*1e3,(F3t/np.amax(F3t)),color=[1,0,0],label="3T Ref")
ax1.semilogy(VDD3t*1e3,(Fhat/np.amax(Fhat)),color=[1,0,1],label="3T Order 2 Fit")

ax1.set_title("Frequency Versus VDD  @ T = -20 [C]")
ax1.set_ylabel("Normalized Frequency [MHz]")
ax1.set_xlabel("Voltage [mV]")
ax1.legend()
plt.show()
