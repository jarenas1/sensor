clc
clear
close all
reg_order = 3;
%M = csvread('wang_sensor.csv');
M = csvread('../data/temp_sensor.csv');
T = M(:,1);
vdd = M(:,2)*1e3;
Freq = 1e-6*M(:,3); %% in mu seconds
%Freq = 1./Period;

X = [T vdd];

reg=MultiPolyRegress(X,Freq,reg_order,'figure'); % Gives you your fit.
print -dpng goodness_of_fit.png
figure(3)
% 
% figure(1);
% x0=10;
% y0=10;
% width=1100;
% height=400;
% set(gcf,'position',[x0,y0,width,height])
% font = 12;
% alw = 1.8;
% fsz = 10;
% lw = 1; %Linewidth
% msz = 8;

colors = [0 0 255;... %Blue
          255 0 0; ... %Red
          0 0 0;...%Black
          44 189 110;...%Green
          144 44 90;... %Purple
          234 112 13;...%Orange
          0 176 240;...%Light Blue
          127 127 127;...%Grey
          112 48 160;...%Light Purple
          ]/255;
      

% ax = subplot(1,1,1);
% for i=1:1:num_plots
%     plot(M(:,1),M(:,i+1),'--','Linewidth',lw,'Color',colors(i,:));
%     pos = get(gcf, 'Position');
%     set(gcf, 'Position', [pos(1) pos(2) width, height]);
%     set(gca, 'FontSize', fsz, 'LineWidth', alw, 'fontweight', 'bold');
%     %title(y_label(i),'FontSize',24,'fontname','times');
%     %xlabel(strcat(x_axis," [",unit_letter_x,x_units,"]"),'FontSize'...
%         %,18,'fontname','times');
%     %ylabel(strcat(y_label(i)," [",unit_letter,y_units(i),"]"),...
%         %'FontSize',18,'fontname','times');
%     ax.XAxis.FontSize = 18;
%     ax.XAxis.FontName = 'times';
%     ax.YAxis.FontSize = 18;
%     ax.YAxis.FontName = 'times';
%     %legend(legend_var,'Location','best','LineWidth',2,...
%         %'FontWeight','bold','FontSize',18,'FontName','times');
%     hold on
% end

temp_coef = reg.Coefficients;
t = temp_coef;
V = vdd*1e3;
[xp,yp] = meshgrid(T,vdd*1e3);
%%%For two order regression
%Freq_fit = t(4) + t(5).*(T.^2) + t(2).*T + t(6).*(V.^2)...
%  + t(1).*V + t(3).*(T.*V);
Freq_fit = reg.yhat;
%%% For third order regression
% Freq_fit = t(8) + t(1).*V + t(2).*(V.^2) + t(3).*T + t(4).*(T.*V)...
%     + t(5).*(T.*V.*V) + t(6).*(T.^2) + t(7).*(T.*T.*V) + ...
%     t(9).*(T.^2)+t(10).*(V.^2);

dt = delaunayTriangulation(T,V) ;
tri = dt.ConnectivityList ;
figure
trisurf(tri,T,V,Freq_fit,'facecolor',[215, 227, 240]/255,...
    'facealpha',0.5,'edgecolor',[215, 227, 240]/255)


error_fit = (Freq-Freq_fit);
rel_error_fit = 100.*error_fit./Freq;

hold on

trisurf(tri,T,V,Freq,'facecolor',[255, 213, 213]/255,'facealpha',0.5,'edgecolor',[255, 213, 213]/255)



title(strcat('Fit vs Real Data @ n = ',string(reg_order))...
    ,'fontsize',24);
xlabel('Temperature [^\circ C]');
ylabel('VDD [mV]');
zlabel('Frequency [MHz]');
legend('Fit Function','Real Data','location','best')

print -dpng surfaces.png


figure(3)
trisurf(tri,T,V,rel_error_fit,'facecolor',[215, 227, 240]/255,'edgecolor'...
    ,[215, 227, 240]/255)

title(strcat('Relative Error (%) @ n = ',string(reg_order))...
    ,'fontsize',24);
xlabel('Temperature [^\circ C]');
ylabel('VDD [mV]');
zlabel('Relative Error [%]');
legend(strcat('Max Error = ',string(max(abs(rel_error_fit))),'%'),'location'...
    ,'best');
print -dpng error.png
%Freq_fit_mat = meshgrid(Freq_fit);
%error_mat = meshgrid(error_fit);
%rel_error_mat = meshgrid(rel_error_fit);
%error_mat = Freq_mat - Freq_fit_mat;
%rel_error_mat = 100.*error_mat./Freq_nom;
%surf(xp,yp,Freq_fit_mat,'EdgeColor','black','FaceColor',[215, 227, 240]/255)
%plot(Freq,(reg.yhat-Freq)./Freq,'*')
hold on
%plot3(xp,yp,Freq_mat,'*','Color','r');
