VDDMIN = 600e-3
VDDMAX = 1
VPOINTS = 10
TMIN = -40
TMAX = 125
TPOINTS = 10
TARGET = frequency
TSAMPLE = 0
SAMPLES = 200
CORES = 10
SENSOR=temp_sensor_ro
CTL = spice/$(SENSOR)
MT = spice/outputs/$(SENSOR)
CSV = data/$(SENSOR)_new_feb
COEF = $(CSV)_coef
TXT = spice/outputs/temp_vdd.txt
APOINTS = 12
SPOINT = 0
FIRSTRUN = 1
REGORDER = 3
DEBUG_SAMPLE = 1


clean:
	@-rm -rf spice/outputs/*

hspice: $(TXT) $(CTL).ctl
	hspice64 -mt $(CORES) -i $(CTL).ctl -o spice/outputs/


typ: $(CTL).ctl
	mkdir -p data spice/outputs
	python3 python/datagen.py -vmin $(VDDMIN) -vmax $(VDDMAX) -vpoints $(VPOINTS) -Tmin $(TMIN) -Tmax $(TMAX) -Tpoints $(TPOINTS) -target_var $(TARGET) -ctl_file $(CTL).ctl -mc_file $(MT).mt0 -cores $(CORES) -csv_file $(CSV).csv -msamples 0 -asamples 0 -start_sample $(SPOINT) -firstrun 1


montecarlo: $(CTL).ctl
	python3 python/datagen.py -vmin $(VDDMIN) -vmax $(VDDMAX) -vpoints $(VPOINTS) -Tmin $(TMIN) -Tmax $(TMAX) -Tpoints $(TPOINTS) -target_var $(TARGET) -ctl_file $(CTL).ctl -mc_file $(MT).mt0 -cores $(CORES) -csv_file $(CSV).csv -msamples $(SAMPLES) -asamples 0 -start_sample $(SPOINT) -firstrun $(FIRSTRUN)

	 
aging: $(CTL).ctl
	python3 python/datagen.py -vmin $(VDDMIN) -vmax $(VDDMAX) -vpoints $(VPOINTS) -Tmin $(TMIN) -Tmax $(TMAX) -Tpoints $(TPOINTS) -target_var $(TARGET) -ctl_file $(CTL).ctl -mc_file $(MT).mt0 -cores $(CORES) -csv_file $(CSV)_aging.csv -msamples 0 -asamples $(APOINTS) -start_sample $(SPOINT) -firstrun 1


coef: $(CTL).ctl
	python3 python/customfit.py -csv_file $(CSV).csv -coef_file $(COEF).csv -reg_order $(REGORDER) -target $(TARGET)

split: $(CTL).ctl
	python3 python/splitfit.py -csv_file $(CSV).csv -coef_file $(COEF).csv

surfplot:
	python3 python/surfplot.py -in_file $(CSV).csv -target_sample $(TSAMPLE)

ageplot:
	python3 python/agingplot.py -in_file $(CSV)_aging.csv -target_sample $(TSAMPLE)

debug:
	python3 python/debug_csv.py -csv_in $(CSV).csv -target_sample $(DEBUG_SAMPLE) -csv_out $(CSV)_sample_$(DEBUG_SAMPLE).csv

