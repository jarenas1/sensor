*line one of a control file must be a comment (HSPICE syntax rules)
.INC "outputs/temp_vdd.txt"
.INC "../netlist/LC_temp_sensor_ro.ckt"
* ==========================
*   Simulation Parameters
* ==========================
.PARAM vss =0
Vvsupply VDD VSS vdd
Vvss VSS 0 vss

*.ic V(vsensor)=vdd
*.ic V(vref)=200e-3

Vvh VINIT 0 PWL(0 0 10n 0 10.01n vdd)

* Generate the lis file correctly
.OPTION MEASFORM = 1
.OPTION BRIEF
*.OPTION POST
*.OPTION MTTHRESH=3

 

*.MEAS TRAN vo_max MAX V(Vout) from 0 to 'tsim/6'
 *measure the comparator signal period

*.MEAS vo_avg AVG V(vsensor) from 'tsim/6' to 'tsim/3'
.MEAS TRAN tdelay
        + TRIG V(Vout) VAL='vdd*0.4' TD=0 RISE = 5
        + TARG V(Vout) VAL='vdd*0.4' RISE = 10

.MEAS TRAN frequency PARAM  = '5/tdelay'
.MEAS TRAN vref AVG V(vbias) from tdelay/2 to tdelay
*.MEAS TRAN vth AVG lv9(xi1.m0) from 0 to tdelay
*.MEAS TRAN vov AVG lv10(xi0.m0) from 0 to tdelay
*.MEAS TRAN ids MAX lx4(xi0.m0) from 0 to tdelay
*.MEAS TRAN vds MAX lx3(xi0.m0) from 0 to tdelay

*.MEAS TRAN vth_lvt AVG lv2(xi1.m0) from 0 to tdelay
*.MEAS TRAN vth_hvt AVG lv2(xi1.m2) from 0 to tdelay
*.MEAS TRAN delta_vth PARAM= vth_hvt/vth_lvt


.OPTION AUTOSTOP='tdelay'
*.print lv9(xi1.m0)
.probe V(*) I(*)
.END

