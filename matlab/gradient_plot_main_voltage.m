clc
clear
close all
reg_order = 3;
M = csvread('../data/voltage_sensor_ro_lvt.csv');
M2 = csvread('../data/sensor_data.csv');
T = M(:,1);
vdd = M(:,2)*1e3;

Freq_voltage = 1e-6*M(:,3); %% in mu seconds
Freq_temp = 1e-6*M2(:,3);
%Freq = 1./Period;

dt = (max(T)-min(T))/length(unique(T));
dv = (max(vdd)-min(vdd))/length(unique(vdd));
X = [T vdd];

m = length(Freq_voltage);
[Tg, Vg] = meshgrid(T,vdd);

uM = griddata(T,vdd,Freq_voltage,Tg,Vg);
uM2 = griddata(T,vdd,Freq_temp,Tg,Vg);

[GFreqTM,GFreqVM]= gradient(uM,dt,dv);
[GFreqTM2,GFreqVM2]= gradient(uM2,dt,dv);

figure(2)
[~,worst_cases_vdd] = max(GFreqVM);
[~,worst_cases_temp] = max(abs(GFreqVM));
%ex_index = randi(length(T),1,10);
%ex_index = worst_cases_vdd(randi([1 length(worst_cases_vdd)],1,10));
ex_index = worst_cases_temp(randi([1 length(worst_cases_temp)],1,10));
T_plot = T(ex_index)';
vdd_plot = vdd(ex_index)';
GFreqT_plotM = GFreqTM(ex_index);
GFreqV_plotM = GFreqVM(ex_index);
GFreqT_plotM2 = GFreqTM2(ex_index);
GFreqV_plotM2 = GFreqVM2(ex_index);


quiver(T_plot,vdd_plot,GFreqT_plotM,GFreqV_plotM,'maxheadsize',0.4,'linewidth',1,'color','r');
hold on
quiver(T_plot,vdd_plot,GFreqT_plotM2,GFreqV_plotM2,'maxheadsize',0.4,'linewidth',1,'color','b');
legend("LVT RO","Current Starved RO",'Location','best');
xlabel("Temperatue [^\circ C]")
ylabel("Supply Voltage [mV]")
title("Gradient Sensor Sensitivity")
print -dpng gradient.png