import numpy as np
import numpy.matlib
import os
import argparse
import time
################################################################################################
##This function reads a .mt0 file and collects the "target_var" values.
def parse_out(input_file,target_var):
    target_index  = 2 #Default value to get the frequency
    with open(input_file) as f:
        lines = f.readlines()
    
    flag = False
    counter = 0
    f.close()
    matrix = np.empty([1,1])
    for i in lines:
        a = i.split()
        if(a[-1]=='alter#'): #This means that here I will see the declaration of the variable
            for k in range(0,len(a)):
                if(a[k] == target_var):
                    target_index = k
                    break
                else:
                    pass
        try:
            a = [float(j) for j in a]
            frequency = np.array(a[target_index])
            #print(frequency)
            if flag == False:
                matrix = frequency
                flag = True
            else:
                matrix = np.append(matrix,frequency)
                counter = counter + 1
        except:
            pass
    return matrix 

################################################################################################

############################# MAIN PROGRAM ######################
parser= argparse.ArgumentParser()

parser.add_argument("-vmin","--VDD_min",required=True, help="VDD min [V]")
parser.add_argument("-vmax","--VDD_max",required=True, help="VDD max [V]")
parser.add_argument("-vpoints","--VDD_points",required=True, help="VDD points")
parser.add_argument("-Tmin","--TEMP_min",required=True, help="TEMP min [C]")
parser.add_argument("-Tmax","--TEMP_max",required=True, help="TEMP max [C]")
parser.add_argument("-Tpoints","--TEMP_points",required=True, help="TEMP points")
parser.add_argument("-msamples","--MONTE_samples",required=True,help="Montecarlo Samples")
parser.add_argument("-target_var","--target_var",required=True, help="hspice var to get")
parser.add_argument("-ctl_file","--ctl_file",required=True, help="hspice control file")
parser.add_argument("-mc_file","--mc_file",required=True, help="hspice mt0 file")
parser.add_argument("-cores","--cores",required=True, help="number of cores for hspice simulation")


args = vars(parser.parse_args())

VDD_min = float(args["VDD_min"])
VDD_max = float(args["VDD_max"])
VDD_points = int(args["VDD_points"])
TEMP_min = float(args["TEMP_min"])
TEMP_max = float(args["TEMP_max"])
TEMP_points = int(args["TEMP_points"])
MONTE = int(args["MONTE_samples"])
target_var = str(args["target_var"])
mc_file = str(args["mc_file"])

t = time.time()


if(MONTE>0):
    run_hspice = "hspice64 -mp "+ str(args["cores"]) +" -i " + str(args["ctl_file"] + " -o spice/outputs/ eclipse >/dev/null")
else:
    run_hspice = "hspice64 -mt "+ str(args["cores"]) +" -i " + str(args["ctl_file"] + " -o spice/outputs/ eclipse >/dev/null")


VDD = np.linspace(VDD_min,VDD_max,VDD_points)

TEMP = np.linspace(TEMP_min,TEMP_max,TEMP_points)
TEMP = np.matlib.repmat(TEMP,1,VDD_points)
TEMP = TEMP.flatten()

VDD = np.linspace(VDD_min,VDD_max,VDD_points)
VDD = np.matlib.repmat(VDD,1,TEMP_points)
VDD = np.sort(VDD.flatten())

fsim = 17.95 + 0.006*(TEMP*TEMP) + 0.001*TEMP
Tsim = 1e-6/fsim
tsim = 30*Tsim

flag = False #to avoid concatenations with empty arrays
counter = 1

for i in range(0,VDD.size):

    #This creates the file read by the hspice ctl file.
    #The syntax includes TEMP, VDD, and tsim as parameters.
    
    data = open("spice/outputs/temp_vdd.txt","w")
    #data.write(".param temp = "+str(TEMP[i])+"\n")
    data.write(".TEMP "+str(TEMP[i])+"\n")
    data.write(".param vdd = "+str(VDD[i])+"\n")
    data.write(".param tsim = " + str(tsim[i])+"\n")

    if(MONTE>0):
        data.write(".TRAN 0 tsim 'tsim/0.5e2' sweep monte="+str(MONTE))
    else:
        data.write(".TRAN 0 tsim 'tsim/0.5e2'") 
    data.close()
    
    os.system(run_hspice) #run hspice
    matrix = parse_out(mc_file,target_var) #once hspice is done, collect the matrix
    T_V = np.array([TEMP[i],VDD[i]]) #Create the TEMP,VDD matrix
    
    array_line = np.insert(T_V,2,matrix) #Create the TEMP,VDD,frequency matrix
    #The 2 reffers to insert the matrix in the column number two
    #Column 0 is temp, Column 1 is VDD, column 2 is the matrix   
    
    #This flag only happens during the first run, as np can not concatenate
    #empty arrays.
    if flag== False:
        array_matrix = array_line
        flag = True
    else:
        array_matrix = np.concatenate((array_matrix,array_line),axis=0) 
        #Concatenate the matrix with the previous run matrix
        counter = counter + 1 #count how many times I am concatenating
    
    print("Run # "+str(i+1)+" done... "+str(VDD.size-i-1)+" Runs Left") #Print runs left
    
    #Remove temporal files in the folder
    os.system("rm spice/outputs/*.tr0* spice/outputs/*.mc0 spice/outputs/*.mpp0 spice/outputs/*.pa0 spice/outputs/*.st0 spice/outputs/*.su0 spice/outputs/*.ic0 spice/outputs/*.json spice/outputs/*.pdf spice/outputs/*.log spice/outputs/*logFile eclipse >/dev/null 2>&1")    #os.system("rm spice/outputs/ -rf")
print(time.time()-t)
#print(array_matrix)
array_matrix = array_matrix.reshape(counter,2+matrix.size)
np.savetxt('data/sensor_data.csv',array_matrix,delimiter=",",fmt='%1.5e')
