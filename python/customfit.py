import numpy as np
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
import sys
from scipy.optimize import minimize
import argparse

def mat_D(Freq):
    #Create the Matrix D for RLS
    Period = 1/Freq # We need the multiplicative inverse of Freq
    n=np.shape(Freq)[0]
    D = np.zeros((n,n))
    Period = np.array(Period)
    #cost = np.linspace(1,0.5,num=Period.shape[0])
    np.fill_diagonal(D,Period)
    return D

def LS(X,Freq):
    M1 = np.matmul(np.transpose(X),X)
    M2 = np.matmul(np.transpose(X),Freq)
    beta = np.matmul(np.linalg.inv(M1),M2)
    return beta

def RLS(X,Freq):
    #Define the matrix D
    D = mat_D(Freq)
    M1 = np.matmul(np.matmul(np.transpose(X),np.matmul(D,D)),X)
    M2 = np.matmul(np.matmul(np.transpose(X),np.matmul(D,D)),Freq)
    beta = np.matmul(np.linalg.inv(M1),M2)
    return beta

def RIDGE_RLS(X,Freq,alpha):
    #Define the matrix D
    D = mat_D(Freq)
    M1 = np.matmul(np.matmul(np.transpose(X),np.matmul(D,D)),X)+alpha
    M2 = np.matmul(np.matmul(np.transpose(X),np.matmul(D,D)),Freq)
    beta = np.matmul(np.linalg.inv(M1),M2)
    return beta

def eval_coeff(X,beta):
    return np.matmul(X,beta)

def eval_error(Freq,Freq_hat):
    abs_error = np.absolute(Freq - Freq_hat)
    rel_error = abs_error/Freq
    return 100*np.amax(rel_error)


def gof(Freq,Freq_hat_ls,Freq_hat_rls):
    fig,ax = plt.subplots(figsize=(10,6))
    obs = ax.plot(Freq,Freq,color="black")
    est_ls = ax.plot(Freq,Freq_hat_ls,color="red",marker='.',markersize=0.1,label="LS Regression")
    est_rls = ax.plot(Freq,Freq_hat_rls,color="blue",marker='.',markersize=0.1,label="RLS Regression")
    ax.set_xlim(min(np.amin(Freq),np.amin(Freq_hat)),max(np.amax(Freq),np.amax(Freq_hat)))
    ax.set_ylim(min(np.amin(Freq),np.amin(Freq_hat)),max(np.amax(Freq),np.amax(Freq_hat)))
    ax.set(title = "Frequency GOF",
            xlabel="Observable Frequency[MHz]",
            ylabel="Estimated Frequency[MHz]")
    axes = plt.gca()
    axes.title.set_size(24)
    axes.xaxis.label.set_size(18)
    axes.yaxis.label.set_size(18)
    ax.legend() 
    
    plt.savefig('gof.png')
    plt.show()
    return 0

def error_plot(Freq,Freq_hat_ls,Freq_hat_rls):
    error_ls = (Freq - Freq_hat_ls)/Freq * 100
    error_rls = (Freq - Freq_hat_rls)/Freq * 100
    
    fig,(ax,ax2) = plt.subplots(1,2,figsize=(10,6))
    est_ls = ax.scatter(Freq,error_ls,color="red",label="LS Regression")
    est_rls = ax.scatter(Freq,error_rls,color="blue",label="RLS Regression")
    ax.set_xlim(np.amin(Freq),np.amax(Freq))
    ax.set_ylim(min(np.amin(error_ls),np.amin(error_rls)),max(np.amax(error_ls),np.amax(error_rls)))
    ax.set(title = "RE Scatter Plot",
            xlabel="Obs Freq[MHz]",
            ylabel="RE[%]")
    ax.legend() 
    
    est_ls = ax2.scatter(Freq,np.abs(error_ls),color="red",label="LS Regression")
    est_rls = ax2.scatter(Freq,np.abs(error_rls),color="blue",label="RLS Regression")
    ax2.set_xlim(np.amin(Freq),np.amax(Freq))
    ax2.set_ylim(min(np.amin(np.abs(error_ls)),np.amin(np.abs(error_rls))),max(np.amax(np.abs(error_ls)),np.amax(np.abs(error_rls))))
    ax2.set(title = "Abs RE Scatter Plot",
            xlabel="Obs Freq[MHz]",
            ylabel="RE[%]",)
    ax2.legend() 


    plt.savefig('error_plot.png')
    plt.show()
    return 0

parser = argparse.ArgumentParser()

parser.add_argument("-csv_file","--csv_file",required=True, help="input csv file")
parser.add_argument("-coef_file","--coef_file",required=True, help="coefficients csv file")

args = vars(parser.parse_args())

csv_file = str(args["csv_file"])
coef_file = str(args["coef_file"])
obs_data = np.loadtxt(csv_file,dtype=float,delimiter=",")
iterations = np.shape(obs_data)[0]
samples = np.shape(obs_data)[1] - 2
TEMP = obs_data[:,0]
VDD = obs_data[:,1]*1e3

#Freq = np.log2(obs_data[:,2:]*1e-6)
Freq = obs_data[:,2:]*1e-6

X = obs_data[:,0:2]
X[:,1] = X[:,1]*1e3

flag = False
#Create the polynomial matrix using the SkLearn Tools.
#The order of the coefficients is in the head string

poly = PolynomialFeatures(degree=3)
Xpoly = poly.fit_transform(X)
print(TEMP)
print(VDD)
print(Xpoly[0,:])
head = "A0,T,V,T^2,TV,V^2,T^3,T^2V,TV^2,V^3"

#The linear model is Freq = Xpoly*Beta + error
worst_error_old = 0
for i in range(0,samples):
    
    #Least Squares
    beta_ls = LS(Xpoly,Freq[:,i])
    Freq_hat_ls = eval_coeff(Xpoly,beta_ls)
    worst_error_ls = eval_error(Freq[:,i],Freq_hat_ls)
    
    #Relative Least Squares
    beta_rls = RLS(Xpoly,Freq[:,i])
    Freq_hat_rls = eval_coeff(Xpoly,beta_rls)
    worst_error_rls = eval_error(Freq[:,i],Freq_hat_rls)
    
    if(worst_error_rls <= worst_error_ls):
        beta = beta_rls
        worst_error = worst_error_rls
        Freq_hat = Freq_hat_rls
    else:
        beta = beta_ls
        worst_error = worst_error_ls
        Freq_hat = Freq_hat_ls
    if(worst_error >= worst_error_old):
        worst_error_old = worst_error
        worst_sample = i
    
    print("Sample No " + str(i+1) + " Worst Error = " +str(worst_error)+"%")
    #print("Sample No " + str(i) + " LS Error = " +str(worst_error_ls)+"%")
    #print("Sample No " + str(i) + " RLS Error = " +str(worst_error_rls)+"%")
    if flag== False:
        np.savetxt(coef_file,[beta],delimiter=",",fmt='%1.5e',header=head)
        flag = True
    else:
        with open(coef_file,'ab') as f:
            np.savetxt(f,[beta],delimiter=",",fmt='%1.5e')
        f.close()

##Plot of the worst sample
Freq_eval = Freq[:,worst_sample]

beta_ls = LS(Xpoly,Freq_eval)
Freq_eval_ls = eval_coeff(Xpoly,beta_ls)
error_ls = eval_error(Freq_eval,Freq_eval_ls)

beta_rls = RLS(Xpoly,Freq_eval)
Freq_eval_rls = eval_coeff(Xpoly,beta_rls)
error_rls = eval_error(Freq_eval,Freq_eval_rls)
#worst_obs = Freq_eval[np.where(error_rls==np.amax(error_rls))]
#worst_est = Freq_eval_rls[np.where(error_rls==np.amax(error_rls))]

print("The worst error was " + str(round(worst_error_old,2)) + "% in the sample " + str(worst_sample+1))
print("T = " + str(TEMP[worst_sample]) + " C, VDD = " + str(VDD[worst_sample]) + " mV")
error_plot(Freq_eval,Freq_eval_ls,Freq_eval_rls)
