import numpy as np
import numpy.matlib
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from scipy.interpolate import griddata
import argparse

parser= argparse.ArgumentParser()
parser.add_argument("-in_file","--in_file",required=True, help="input_file")
parser.add_argument("-target_sample","--target_sample",required=False,help="target sample that you want to plot in MC. By default is 0 (typical)")
args = vars(parser.parse_args())

in_file = str(args["in_file"])
target_sample = int(args["target_sample"])


M = np.loadtxt(in_file,delimiter=",")
M_shape = M.shape
num_samples = M.shape[1]-2
TEMP = M[:,0]
VDD = M[:,1]
GRID = M[:,0:2]
if(num_samples>1):
    FREQ = M[:,2:num_samples]*1e-6
    TARGET_FREQ = FREQ[:,target_sample]
else:
    FREQ = M[:,2]*1e-6
    TARGET_FREQ = FREQ
grid_T,grid_V = np.meshgrid(TEMP,VDD)


F_grid = griddata(GRID,TARGET_FREQ,(grid_T,grid_V),method='nearest')

## PLOT ##
fig,ax = plt.subplots(subplot_kw={"projection":"3d"},figsize=(10,6))

surf = ax.plot_surface(grid_T,grid_V,F_grid,linewidth=0,cmap=cm.coolwarm)
ax.set_xlim(-20,130)
ax.set_ylim(0.6,1.0)
ax.set(title = "Frequency vs T and VDD",
        xlabel="Temperature [C]",
        ylabel="VDD [V]",
        zlabel="Frequency [MHz]")
axes = plt.gca()
axes.title.set_size(24)
axes.xaxis.label.set_size(18)
axes.yaxis.label.set_size(18)
axes.zaxis.label.set_size(18)


fig.colorbar(surf, shrink=0.5, aspect=5)

plt.savefig('surface.png')
plt.show()




