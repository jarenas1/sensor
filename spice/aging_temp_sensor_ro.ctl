*line one of a control file must be a comment (HSPICE syntax rules)
* ==========================
*      File Includes
* ==========================



.INC "outputs/temp_vdd.txt"
.INC "../netlist/temp_sensor_ro.ckt"


* ==========================
*   Simulation Parameters
* ==========================
* - essentially variables
* - HSPICE keywords are case insensitive (.param, .PARAM, etc.)
.PARAM vss =0
.PARAM tsim = 20u
*.PARAM tsim = 10u
Vvsupply VDD VSS vdd
Vvss VSS 0 vss
.ic V(VOUT)=vdd
*.ic V(vref)=200e-3
* ==========================
*    Simulation Duration
* ==========================
* - transient simulation


* Generate the lis file correctly
.OPTION MEASFORM = 1
*.OPTION BRIEF
.OPTION POST
.OPTION MTTHRESH=3

*.MEAS TRAN vo_max MAX V(Vout)
 *measure the comparator signal period

.MEAS vo_avg AVG V(Vout) from 'tsim/6' to 'tsim/3'
.MEAS TRAN tdelay
        + TRIG V(Vout) VAL='vo_avg' TD='tsim/3' RISE = 1
        + TARG V(Vout) VAL='vo_avg' RISE =31

.MEAS TRAN frequency PARAM  = '30/tdelay'
.OPTION AUTOSTOP='tdelay'
.model pmos_age mosra level=1
+tit0 = 5e-8 titfd = 7.5e-10 tittd = 1.45e-20
+tn = 0.25 udcd = 0.5


.option appendall
.appendmodel pmos_age mosra pch_lvt pmos

.mosra
+RelTotalTime='10*32e6'
+RelStep='1*32e6'



.END
