clc
clear
close all
reg_order = 3;
M = csvread('../data/observed_data_ro.csv');

T = M(:,1);
vdd = M(:,2)*1e3;

Freq = 1e-6*M(:,3); %% in mu seconds
%Freq = 1./Period;

dt = (max(T)-min(T))/length(unique(T));
dv = (max(vdd)-min(vdd))/length(unique(vdd));
X = [T vdd];

m = length(Freq);
[Tg, Vg] = meshgrid(T,vdd);

u = griddata(T,vdd,Freq,Tg,Vg);

[GFreqT,GFreqV]= gradient(u,dt,dv);
[~,worst_cases_vdd] = max(GFreqV);


ex_index = worst_cases_vdd(randi([1 length(worst_cases_vdd)],1,5));

T0= X(ex_index,1);
V0= X(ex_index,2);

t = (T == T0) & (vdd == V0);
indt = find(t);


f_grad = [GFreqT(indt) GFreqV(indt)];


%%% Create the axis %%%%

%%% Horizontal and Vertical Lines %%%
P1 = [T(indt) vdd(indt)];
P2 = P1+f_grad;

dp = f_grad;
%quiver(P1(1),P1(2),dp(1),dp(2),0,'maxheadsize',0.01,'linewidth',2);

%text(P1(1),P1(2), sprintf('(%.0f,%.0f)',P1))
%text(P2(1),P2(2), sprintf('(%.0f,%.0f)',P2))
% annotation('arrow',HL/200,VL(1)*ones(1,2)/max(vdd));
% hold on
% annotation('arrow',HL(1)*ones(1,2)/200,VL/max(vdd));
%plot(HL,VL(1)*ones(1,2),'--','linewidth',1,'color','black');
%hold on
%plot(HL(1)*ones(1,2),VL,'--','linewidth',1,'color','black');



%xlim([120-20,max(T)+20]);
%ylim([min(vdd)-100,max(vdd)+100]);

