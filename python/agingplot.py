import numpy as np
import numpy.matlib
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from scipy.interpolate import griddata
import argparse

parser= argparse.ArgumentParser()
parser.add_argument("-in_file","--in_file",required=True, help="input_file")
parser.add_argument("-target_sample","--target_sample",required=False,help="target sample that you want to perform the aging plot. By default is 0 (first sample in the CSV)")
args = vars(parser.parse_args())

in_file = str(args["in_file"])
target_sample = int(args["target_sample"])


M = np.loadtxt(in_file,delimiter=",")
M_shape = M.shape
num_samples = M.shape[1]-2
TEMP = M[target_sample,0]
VDD = M[target_sample,1]*1e3
FREQ = M[target_sample,:][2:]*1e-6

YEARS = np.linspace(0,num_samples-1,num=num_samples)
print(YEARS)
## PLOT ##

fig, ax = plt.subplots(figsize=(10,6))

ax.plot(YEARS,FREQ)
title_str ="Sensor Aging Profile @ T = " + str(TEMP) +"[C], VDD = "+str(VDD)+" [mV]"
ax.set(title = title_str,
        xlabel="time [years]",
        ylabel="Frequency [MHz]")
axes = plt.gca()
axes.title.set_size(20)
axes.xaxis.label.set_size(18)
axes.yaxis.label.set_size(18)

plt.savefig('aging.png')
plt.show()


