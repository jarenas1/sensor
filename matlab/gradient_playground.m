clc
clear
close all
reg_order = 3;

M = csvread('../data/voltage_sensor_ro_lvt.csv');
M2 = M;
M = sortrows(M,2);
T = M(:,1);
vdd = M(:,2)*1e3;
Freq = 1e-6*M(:,3); %% in mu seconds
%Freq = 1./Period;

%%%% NORMALIZATION %%%%%

% T_pos = T - min(T);
% T_pos = T_pos/max(T);
T = T/max(abs(T));
vdd = vdd/max(vdd);
Freq = Freq/max(Freq);

dfdv = diff(Freq)./diff(vdd);

[Tg,Vg] = meshgrid(T,vdd);

uM = griddata(T,vdd,Freq,Tg,Vg);

[pT,pV] = gradient(uM,3.0612,8.1630);

%[pT,pV] = gradient(uM);

figure(1)
%sc = surf(Tg,Vg,gradient(uM,3.0612,608.1630));
sc=surf(Tg,Vg,uM);
figure(2)
%sc2=surf(Tg,Vg,gradient(uM));
%zlim([min(Freq)*0.9,max(Freq)*1.1]);
%sc(2).ZLocation = 'zmax';
%colorbar
%hold on
%quiver(Tg,Vg,pT,pV);
contour(Tg,Vg,uM);


%T0 = T(20);
%V0 = vdd(50);

%t = (T == T0) & (vdd == V0);
%indt = find(t);
%f_grad = [pT(indt) pV(indt)];
%display(f_grad);

%%
%figure(1)
%quiver(T,vdd,pT,pV);

[~,worst_cases_vdd] = max(abs(pV));
%ex_index = worst_cases_vdd(1:10);
ex_index = randi(2500,1,10);
%find(T>0.9 & T<0.92 & vdd>0.85)
T_plot = T(ex_index)';
vdd_plot = vdd(ex_index)';


pT_plot = pT(ex_index);
pV_plot = pV(ex_index);

figure(3)
%contour(Tg,Vg,uM);
%hold on
%quiver(T_plot,vdd_plot,pT_plot,pV_plot,2);
quiver(T,vdd,pT,pV,100);
