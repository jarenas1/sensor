function [Q1,Q2,Q3,Q4,T1,V1] = fit_quadrants(T,V,F,T2,V2,delta,flag_plot)
clc
close all

%%% Create unique value vectors %%%
Tu = unique(T);
Vu = unique(V);

%%% Define the T and V intervals %%%
total_delta_T = max(Tu)-min(Tu);
total_delta_V = max(Vu)-min(Vu);

delta_T = delta*total_delta_T;
delta_V = delta*total_delta_V;

%%% Separate the unique vector two %%%
V1 = delta_V + V2;
T1 = delta_T + T2;

Vq1 = Vu(find(Vu<=V1));
Vq2 = Vu(find(Vu >= V2));

Tq1 = Tu(find(Tu<=T1));
Tq2 = Tu(find(Tu >= T2));

if(flag_plot == 1)
%     figure(1)
%     axis([min(Tu) max(Tu) min(Vu) max(Vu)])
%     rectangle('position',[min(Tu) V2 T1-min(Tu)  max(Vu)-V2],'facecolor'...
%         ,[0,0,255]/255)
%     figure(2)
%     axis([min(Tu) max(Tu) min(Vu) max(Vu)])
%     rectangle('position',[T2 V2 max(Tu)-T2  max(Vu)-V2],'facecolor'...
%         ,[255,0,0,120]/255)
%     figure(3)
%     axis([min(Tu) max(Tu) min(Vu) max(Vu)])
%     rectangle('position',[min(Tu) min(Vu) T1-min(Tu)  V1-min(Vu)],'facecolor'...
%         ,[0,176,80,120]/255)
%     figure(4)
%     axis([min(Tu) max(Tu) min(Vu) max(Vu)])
%     rectangle('position',[T2 min(Vu) max(Tu)-T2  V1-min(Vu)],'facecolor'...
%         ,[255,255,0,120]/255)

figure(1)
axis([min(Tu) max(Tu) min(Vu) max(Vu)])
    rectangle('position',[min(Tu) V2 T1-min(Tu)  max(Vu)-V2],'facecolor'...
         ,[0,0,255,125]/255)
    hold on 
    rectangle('position',[T2 V2 max(Tu)-T2  max(Vu)-V2],'facecolor'...
        ,[255,0,0,120]/255)
    rectangle('position',[min(Tu) min(Vu) T1-min(Tu)  V1-min(Vu)],'facecolor'...
    ,[0,176,80,120]/255)
    rectangle('position',[T2 min(Vu) max(Tu)-T2  V1-min(Vu)],'facecolor'...
        ,[255,255,0,120]/255)
    title("Quadrants for piecewise regression");
    xlabel('Temperature [^\circ C]')
    ylabel('Vdd [mV]')
end

%%% For the record. you cant do this as the VDD and teperature arrays
%%% doesnt need to have the same size. That is why you need a for loop.
%X1 = [Tq1 Vq1];
%X2 = [Tq2 Vq2];
t1 = [];
v1 =[];
t2 = t1;
v2 = v1;
%%% Compute the frequencies depending on the quadrant %%%
for i=1:1:length(Tq1)
    t1 = [t1;find(T==Tq1(i))];
end
for i=1:1:length(Vq1)
    v1 = [v1;find(V==Vq1(i))];
end
for i=1:1:length(Tq2)
    t2 = [t2;find(T==Tq2(i))];
end
for i=1:1:length(Vq2)
    v2 = [v2;find(V==Vq2(i))];
end

[IQ1,pos] = intersect(t1,v2);
[IQ2,pos] = intersect(t2,v2);
[IQ3,pos] = intersect(t1,v1);
[IQ4,pos] = intersect(t2,v1);

clear pos
Q1 = [T(IQ1) V(IQ1) F(IQ1)];
Q2 = [T(IQ2) V(IQ2) F(IQ2)];
Q3 = [T(IQ3) V(IQ3) F(IQ3)];
Q4 = [T(IQ4) V(IQ4) F(IQ4)];