*line 1 is a comment


.LIB "/gscratch/ece/pdks/tsmc/N65LP/models/hspice/toplevel.l" tt_lib


m8 VO VI  vdd vdd pch_lvt l=60e-9 w=260e-9 m=1 nf=1 sd=200e-9 ad=45.5e-15 as=45.5e-15 pd=870e-9 ps=870e-9 nrd=384.615e-3 nrs=384.615e-3 sa=175e-9 sb=175e-9 sca=0 scb=0 scc=0


Vvsupply vdd 0 1
r1 VO VD 0

Vvdragin VD 0 0.5
Vvgate VI 0 PULSE(1 0.5 0 100p 100p 4.9n 10n)

.TRAN 1p 30n
.measure tran iavg avg lx4(m8) from=0 to=10n
.model pmos_age mosra level=1
+tit0 = 5e-8 titfd = 7.5e-10 tittd = 1.45e-20
+tn = 0.25

.option appendall
.appendmodel pmos_age mosra pch_lvt pmos

.mosra
+RelTotalTime='100*32e6'
+RelStep='10*32e6'
*.MOSRAPRINT i0 vth(xi0.m0,vds=540e-3,vgs=540e-3,vbs=0)
.print lx4(m8)
.END
