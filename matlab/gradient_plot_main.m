clc
clear
close all
reg_order = 3;
M = csvread('../data/observed_data_ro.csv');

T = M(:,1);
vdd = M(:,2)*1e3;

Freq = 1e-6*M(:,3); %% in mu seconds
%Freq = 1./Period;

dt = (max(T)-min(T))/length(unique(T));
dv = (max(vdd)-min(vdd))/length(unique(vdd));
X = [T vdd];

m = length(Freq);
[Tg, Vg] = meshgrid(T,vdd);

u = griddata(T,vdd,Freq,Tg,Vg);

[GFreqT,GFreqV]= gradient(u,dt,dv);


[~,worst_cases_vdd] = max(GFreqV);
[~,worst_cases_temp] = min(GFreqT);
ex_index = randi(length(T),1,50);
%ex_index = worst_cases_vdd(randi([1 length(worst_cases_vdd)],1,10));
%ex_index = worst_cases_temp(randi([1 length(worst_cases_temp)],1,10));
T_plot = T(ex_index)';
vdd_plot = vdd(ex_index)';
GFreqT_plot = GFreqT(ex_index);
GFreqV_plot = GFreqV(ex_index);
quiver(T_plot,vdd_plot,GFreqT_plot,GFreqV_plot,'maxheadsize',0.5,'linewidth',1,'color','b');
legend("Weak Inv RO",'Location','NorthWest');
xlabel("Temperatue [^\circ C]")
ylabel("Supply Voltage [mV]")
title("Gradient Sensor Sensitivity")
