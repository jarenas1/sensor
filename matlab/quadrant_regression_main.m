clc
clear
close all
reg_order = 3;
reg_order_v = 2;
reg_order_T = 2;
M = csvread('../data/observed_data_ro.csv');

T = M(:,1);
vdd = M(:,2)*1e3;
Freq = M(:,3); %% in mu seconds
%Freq = 1e6/Freq;
V2 = 700;
T2 = 10;
delta = 0.1;

filter = find(T<-120);
T(filter)=[];
Freq(filter)=[];
vdd(filter)=[];
disp(size(T))
[Q1,Q2,Q3,Q4,T1,V1] = fit_quadrants(T,vdd,Freq,T2,V2,delta,1);
print -dpng quadrants.png
%%% Initial 1D regressions %%%
vdd_coeff = polyfit(Freq,vdd,reg_order_v);
vdd_est = polyval(vdd_coeff,Freq);
T_coeff = polyfit(Freq,T,reg_order_T);
T_est = polyval(T_coeff,Freq);

regq1=MultiPolyRegress(Q1(:,1:2),Q1(:,3),reg_order); % Gives you your fit.
regq2=MultiPolyRegress(Q2(:,1:2),Q2(:,3),reg_order); % Gives you your fit.
regq3=MultiPolyRegress(Q3(:,1:2),Q3(:,3),reg_order); % Gives you your fit.
regq4=MultiPolyRegress(Q4(:,1:2),Q4(:,3),reg_order); % Gives you your fit.
%%% Determine in which quadrant I am and perform the regression %%%
for i=1:1:length(Freq)
    flagQ = [false false false false];
     %%% Conditions to be in first quadrant %%%
            %%% vdd_est > V2 ... T_est < T1
            if (vdd_est(i) >= V2 && T_est(i) <= T1)
                flagQ(1)= true;
            end
     %%% Conditions to be in second quadrant %%%
            %%% vdd_est > V2 ... T_est > T2
            if (vdd_est(i) >= V2 && T_est(i) >= T2)
                flagQ(2)= true;
            end
     %%% Conditions to be in third quadrant %%%
            %%% vdd_est < V1 ... T_est < T1
            if (vdd_est(i) <= V1 && T_est(i) <= T1)
                flagQ(3)= true;
            end
     %%% Conditions to be in first quadrant %%%
            %%% vdd_est < V1 ... T_est > T2
            if (vdd_est(i) <= V1 && T_est(i) >= T2)
                flagQ(4)= true;
            end      
     
     %%% Evaluate the regression depending on the quadrant %%%       
     if (flagQ(1)== true)       
        
        f_q1 = regq1.PolynomialExpression(T(i),vdd(i));
     else
        f_q1 = [];
     end
     
     if (flagQ(2)== true)
        
        f_q2 = regq2.PolynomialExpression(T(i),vdd(i));
     else
        f_q2 = [];
      end
      
      
      if (flagQ(3)== true)

        f_q3 = regq3.PolynomialExpression(T(i),vdd(i));
     else
        f_q3 = [];
     end
     
     
     if (flagQ(4)== true)
        
        f_q4 = regq4.PolynomialExpression(T(i),vdd(i));
     else
        f_q4 = [];
     end
     
     
     f_q = [f_q1 f_q2 f_q3 f_q4];
     f_hat(i) = mean(f_q);
     
end

f_hat = f_hat';

error_fit = (Freq-f_hat);
rel_error_fit = 100.*error_fit./Freq;

dt = delaunayTriangulation(T,vdd) ;
tri = dt.ConnectivityList ;

figure(4)
trisurf(tri,T,vdd,f_hat,'facecolor',[215, 227, 240]/255,...
    'facealpha',0.5,'edgecolor',[215, 227, 240]/255)
hold on
trisurf(tri,T,vdd,Freq,'facecolor',[255, 213, 213]/255,'facealpha',0.5,'edgecolor',[255, 213, 213]/255)



title(strcat('Fit vs Real Data @ n = ',string(reg_order))...
    ,'fontsize',24);
xlabel('Temperature [^\circ C]');
ylabel('VDD [mV]');
zlabel('Frequency [MHz]');
legend('Fit Function','Real Data','location','best')

print -dpng surfaces_w_quadrants.png

figure(5)

%[Tg, Vg] = meshgrid(T,vdd);
%u = griddata(T,vdd,rel_error_fit,Tg,Vg);

trisurf(tri,T,vdd,rel_error_fit,'facecolor',[215, 227, 240]/255,...
    'facealpha',0.5,'edgecolor',[215, 227, 240]/255)



title(strcat('Relative Error (%) @ n = ',string(reg_order))...
    ,'fontsize',24);
xlabel('Temperature [^\circ C]');
ylabel('VDD [mV]');
zlabel('Relative Error [%]');
legend(strcat('Max Error = ',string(max(abs(rel_error_fit))),'%'),'location'...
    ,'best');
print -dpng error.png
