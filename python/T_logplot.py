import numpy as np
import matplotlib.pyplot as plt


ideal_source = np.loadtxt("../data/7s_ideal_600mV.csv",dtype=float,delimiter=",")

Tbuff = np.loadtxt("../data/7s_E_600mV.csv",dtype=float,delimiter=",")

Tis = ideal_source[:,0]
VDDis = ideal_source[0,1]
Fis = ideal_source[:,2]

T3t = Tbuff[:,0]
VDD3t = Tbuff[0,1]
F3t = Tbuff[:,2]



fig,ax1 = plt.subplots()

a1=ax1.semilogy(Tis,Fis/np.amax(Fis),color=[0,0,1],label="Ideal Source")
a2=ax1.semilogy(T3t,F3t/np.amax(F3t),color=[1,0,0],label="3T Ref")

ax1.set_title("Frequency Versus Temperature @ VDD = 600 [mV]")
ax1.set_ylabel("Normalized Frequency")
ax1.set_xlabel("Temperature [C]")
ax1.legend()

plt.show()
