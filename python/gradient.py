import numpy as np
import numpy.matlib
import os
import argparse
import time
import matplotlib.pyplot as plt


ftemp = "../data/temp_sensor.csv"
fvoltage = "../data/voltage_sensor_ro_lvt.csv"
M_T = np.loadtxt(ftemp,dtype=float,delimiter=",")
M_V = np.loadtxt(fvoltage,dtype=float,delimiter=",")
T = M_V[:,0]
V = M_V[:,1]*1e3
T_temp = M_T[:,0]
V_temp = M_V[:,1]*1e3
freq_V = M_V[:,2]*1e-6
freq_T = M_T[:,2]*1e-6

print(T)
print(T_temp)
## Normalize the data ##
T = T/np.max(abs(T))
V = V/np.max(V)
T_temp = T_temp/np.max(abs(T_temp))
V_temp = V_temp/np.max(V_temp)
freq_V = freq_V/np.max(freq_V)
freq_T = freq_T/np.max(freq_T)

## Unique points ##
Tu = np.unique(T)
Vu = np.unique(V)
#print(Vu)
delta_T = Tu[1]-Tu[0]
delta_V = Vu[1]-Vu[0]

dfdv_V=[]
dfdT_V=[]

dfdv_T=[]
dfdT_T=[]
## Concatenation of the gradients ##
for i in range(len(Tu)):    
    f0 = freq_V[np.argwhere(T==Tu[i])]
    f0T = freq_T[np.argwhere(T==Tu[i])]

    gv = np.gradient(f0,delta_V,axis=0)
    gvT = np.gradient(f0T,delta_T,axis=0)

    dfdv_V = np.append(dfdv_V,gv)
    dfdv_T = np.append(dfdv_T,gvT)
    
for i in range(len(Vu)):
    f0 = freq_V[np.argwhere(V==Vu[i])]
    f0T = freq_T[np.argwhere(V==Vu[i])]
    gT = np.gradient(f0,delta_T,axis=0)
    gTv = np.gradient(f0T,delta_T,axis=0)
    dfdT_V = np.append(dfdT_V,gT)
    dfdT_T = np.append(dfdT_T,gTv)
    
### Create the grid for the surface plot ###
#Tg,Vg = np.meshgrid(T,V)
#Freqg = np.griddata(T,V,Tg,Vg)


#print(dfdv_V)
#print(V)
#print(freq_V)

print(dfdT_V.size)
plt_points = np.random.randint(dfdT_V.size,size=20)

fig, ax = plt.subplots()

quiv = ax.quiver(T_temp[plt_points],V_temp[plt_points],dfdT_T[plt_points],dfdv_T[plt_points],color=[0,0,1],label="3T RO",units='xy',scale_units='xy',scale=1/0.1)

quiv = ax.quiver(T[plt_points],V[plt_points],dfdT_V[plt_points],dfdv_V[plt_points],color=[1,0,0],label="LVT RO",units='xy',scale_units='xy',scale=1/0.05)

plt.xlabel("Normalized Temperature [C/130C]")
plt.ylabel("Normalized VDD [V/1V]")
plt.title("Gradient Sensor's sensitivity")
plt.ylim([0.6*0.9,1.1])
plt.xlim(0.9*np.amin(T),np.amax(T)*1.1)

ax.legend()

plt.show()

